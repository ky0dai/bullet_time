from scapy.all import *

conf.L3socket
conf.L3socket=L3RawSocket

def get_ack_length(packet):
    flags = packet.flags
    if flags & 8 > 0:
        return len(packet[Raw])
    elif flags & 2 > 0:
        return 1
    else:
        return 0

current_seq = 0

def get_next_seq(outgoing_packet):
    if outgoing_packet.flags & 8 > 0:
        return current_seq + len(outgoing_packet[Raw])
    elif outgoing_packet.flags & 2 > 0:
        return current_seq + 1
    else:
        return current_seq



a = sniff(filter="tcp and port 2113", count =1)

syn = a[0][TCP]

print "ACK INCREMENT SHOULD BE: " + str(get_ack_length(syn))

print "SYN!"

#syn.show()


ip = IP(src=a[0][IP].dst, dst=a[0][IP].src)

print "SYNACK! - Current SEQ will be %s." % current_seq
synack = TCP(flags="SA", sport=syn.dport, dport=syn.sport, seq=0, ack=syn.seq + 1)

#synack.show()

current_seq = get_next_seq(synack)

print "+++++ NEXT SEQ WILL BE %s." % current_seq

ack= sr1(ip/synack)

print('connection established, waiting for push.')

last_ack = syn.seq

while True:
    a = sniff(filter="tcp and port 2113", count =1)
    gotten = a[0][TCP]

    print('>>>>>> RECIEVED -----: %s.' % gotten[Raw])
    payload_length = get_ack_length(gotten)
    print('--------------GOTTEN.SEQ IS: %s.' % gotten.seq)
    print('--------------PAYLOAD_LENGTH IS: %s.' % payload_length)
    print('--------------SEQ should be: %s.' % gotten.ack)
    potato = gotten.seq + payload_length
    print('--------------ACK should be: %s.' % potato)


    pushack = TCP(flags="A", sport=gotten.dport, dport=gotten.sport, seq=gotten.ack, ack=gotten.seq + payload_length)
    current_seq = get_next_seq(pushack)

    send(ip/pushack)










# placeholder
