import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print('connecting...')
sock.connect(('127.0.0.1', 2113))

while True:
    should_exit = raw_input('Say X to exit anything else to send: ')

    if should_exit == 'X':
        break

    print('sending message...')

    #sock.send('message!\r\n')
    sock.send('like\r\n')
    sock.send('another!\r\n')

    should_wait = raw_input('Say W to wait for message: ')

    if should_wait == 'W':
        data = sock.recv(256)
        print('Received: %s' % data)

print('closing socket')
sock.close()
