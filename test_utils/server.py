import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind(('127.0.0.1', 2113))

sock.listen(1)

print('waiting for connection...')
client, address = sock.accept()

print('Got connection from: %s', address)

while True:
    action = raw_input('X to exit or anything else to receive: ')
    if action == 'X':
        break

    message = client.recv(512)

    if message:
        print('received: %s' % message)

    should_reply = raw_input('R to reply: ')

    if should_reply == 'R':
        client.send('received\r\n')

print('done!')
